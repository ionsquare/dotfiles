" For full docs see https://github.com/junegunn/vim-plug
" Commands
" :PlugInstall [name ...] [#threads] 	Install plugins
" :PlugUpdate [name ...] [#threads] 	Install or update plugins
" :PlugClean[!] 	                    Remove unused directories (bang version will clean without prompt)
" :PlugUpgrade 	                      Upgrade vim-plug itself
" :PlugStatus 	                      Check the status of plugins
" :PlugDiff 	                        Examine changes from the previous update and the pending changes
" :PlugSnapshot[!] [output path] 	    Generate script for restoring the current snapshot of the plugins"

" Automatically install vim-plug
if has('nvim')
  if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
else
  if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
endif

call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'JamshedVesuna/vim-markdown-preview'
"Plug 'ctrlpvim/ctrlp.vim'
Plug 'mileszs/ack.vim'
Plug 'airblade/vim-gitgutter'
Plug 'kshenoy/vim-signature'
Plug 'jwalton512/vim-blade'
Plug 'honza/vim-snippets'
Plug 'nanotech/jellybeans.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'vim-syntastic/syntastic'
Plug 'jreybert/vimagit'
Plug 'simnalamburt/vim-mundo'
Plug 'AndrewRadev/linediff.vim'
Plug 'tpope/vim-fugitive'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

call plug#end()

