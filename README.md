# Installation

Quick install:

```bash
curl "https://gitlab.com/matthewhoener/dotfiles/snippets/1727206/raw" | zsh
```

Here's an explanation of what the script in the snippet will run:

```bash
# Clone repo with submodules
cd ~
git init
git remote add -t master origin https://gitlab.com/matthewhoener/dotfiles.git
git pull
git submodule update --init --recursive

# Install vundle plugins
vim -u NONE -S ~/.vim/plugins.vim +PlugInstall +qall

# Install zsh plugins
zsh ~/.zshrc
```

Optionally set zsh as the default shell if it isn't already:

```bash
# Set zsh as default shell
sudo chsh -s /usr/bin/zsh username
```
