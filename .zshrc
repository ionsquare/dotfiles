# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

source ~/.antigen/antigen/antigen.zsh
antigen use oh-my-zsh

# Plugins without special settings
antigen bundle sudo
antigen bundle zsh-completions
antigen bundle colored-man-pages
antigen bundle jocelynmallon/zshmarks
antigen bundle zsh-users/zaw
antigen bundle https://gitlab.com/ionsquare/zsh-plugins.git keybindings
type pygmentize >/dev/null && antigen bundle colorize
type docker     >/dev/null && antigen bundle docker
type ansible    >/dev/null && antigen bundle ansible
type git        >/dev/null && antigen bundle git
type tig        >/dev/null && antigen bundle tig
type task       >/dev/null && antigen bundle taskwarrior

# There's an apparent issue with p10k's instant prompt and this plugins caching
#type composer   >/dev/null && antigen bundle composer

# P9k-related
type git     >/dev/null && antigen bundle https://gitlab.com/ionsquare/zsh-plugins.git p9k-git-branch-shortener
antigen bundle https://gitlab.com/ionsquare/zsh-plugins.git p9k-prompt-switcher

antigen bundle zsh-users/zsh-syntax-highlighting  # zsh-syntax-highlighting must be last
#antigen theme ionsquare/powerlevel9k powerlevel9k
antigen theme romkatv/powerlevel10k
antigen apply

# ===== Syntax Highlighting (must be after `antigen apply`) ====================
typeset -gA ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
ZSH_HIGHLIGHT_STYLES[cursor]='bold'

ZSH_HIGHLIGHT_STYLES[alias]='fg=blue,bold'
ZSH_HIGHLIGHT_STYLES[suffix-alias]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[builtin]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[function]='fg=blue,bold'
ZSH_HIGHLIGHT_STYLES[command]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[precommand]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[hashed-command]='fg=green,bold'

ZSH_HIGHLIGHT_STYLES[path]='fg=159,underline'
ZSH_HIGHLIGHT_STYLES[history-expansion]='fg=12'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=171'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=171'
ZSH_HIGHLIGHT_STYLES[assign]='fg=207'
ZSH_HIGHLIGHT_STYLES[redirection]='fg=81'

# ===== Settings ===============================================================
# DISABLE_UNTRACKED_FILES_DIRTY="true"  # Improve performance of git segment
DISABLE_AUTO_TITLE="true"               # Disable auto-setting terminal title.
COMPLETION_WAITING_DOTS="true"          # Display red dots whilst waiting for completion.
HIST_STAMPS="yyyy-mm-dd"                # Change the command execution timestamp in `history`
autoload -U compinit && compinit        # Load zsh-completions
setopt interactivecomments              # Allow comments in interactive shell
unsetopt HIST_VERIFY                    # Execute line on enter even if there are expansions
export EDITOR='vim'                     # The best editor

# Fix some unreadable colours
export LS_COLORS="rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=91:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:"

# ===== PATH Settings ==========================================================
if [[ -d "$HOME/.config/composer/vendor/bin" ]]; then
  export COMPOSER_HOME="$HOME/.config/composer"
  export PATH="$COMPOSER_HOME/vendor/bin:$PATH"
fi
if [[ -d "$HOME/.local/bin" ]]; then
  export PATH="$HOME/.local/bin:$PATH"
fi

if type "go" >/dev/null 2>&1; then
  export GOPATH=$HOME/go
  export PATH="$PATH:$HOME/go/bin"
fi

# ===== Completions ============================================================
type kubectl > /dev/null && source <(kubectl completion zsh)

# ===== Aliases ================================================================
unalias ls
if [[ $(uname) == "Darwin" ]]; then
  # MacOS Aliases
  type gls >/dev/null && alias ls='gls -p --color=tty'
else
  alias ls='ls -p --color=tty'
fi

# Common Aliases
# General
alias grep='\grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn} --exclude ".*.sw[op]"'
alias ll='\ls -lph --group-directories-first --color=auto'
if [[ -d ~/.config/awesome ]]; then
  alias eA='cd ~/.config/awesome && vim rc.lua'
  alias cA='cd ~/.config/awesome'
fi
alias eZ='cd ~ && vim .zshrc'
alias eV='cd ~ && vim .vimrc'

type nvim >/dev/null && alias nvim="TERM=st-256color \nvim"
if type fzf  >/dev/null; then
  alias gaf='git add $(git status -s | fzf -m | cut -c 4-)'
  alias gafp='git add -p $(git status -s | fzf -m | cut -c 4-)'
  alias gcf='gcof'
  alias gbdf='git branch -d $(git branch | fzf | cut -c 3-)'
  function gcof(){
    if [ -z $1 ]; then
      git checkout $(git branch | fzf | cut -c 3-)
      return
    fi
    git checkout $(gb -r | grep $1 | sed "s@^[^/]*/@@" | fzf --select-1)
  }
fi

# zshmarks aliases
alias j=jump
alias b=bookmark
alias s=showmarks

if type "git" > /dev/null; then
  # Override ghostscript command to do git status instead
  alias gs='git status'
  alias gnb='git --no-pager branch'
  alias gnr='git --no-pager remote'

  if type fzf  >/dev/null; then
    alias gaf='git add $(git status -s | fzf -m | cut -c 4-)'
    alias gafp='git add -p $(git status -s | fzf -m | cut -c 4-)'
    alias gcf='git checkout $(git branch | fzf | cut -c 3-)'
    alias gbdf='git branch -d $(git branch | fzf | cut -c 3-)'
  fi
fi

type nvim       >/dev/null && alias nvim="TERM=st-256color \nvim"   # Set TERM to fix home/end keys
type alsamixer  >/dev/null && alias a="alsamixer"
type irssi      >/dev/null && alias irssi='TERM=screen-256color irssi'

if type task > /dev/null; then
  alias ta='task add +work'
  alias tl='task log +work'
fi

# ===== Other initializations ==================================================
# Initialize fasd - disabled for now because of annoying d alias
#type fasd >/dev/null && eval "$(fasd --init auto)"

# Fuzzy find
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# ===== Functions ==============================================================
sssh(){
  # Keep trying to reconnect
  while true; do command ssh -o ConnectTimeout=10 "$@"; [ $? -eq 0 ] && break || sleep 1; done
}

# Screenshot region to clipboard
scrotclip(){
  scrot -s /tmp/scrotcliptemp.png --select && xclip -selection c -t image/png /tmp/scrotcliptemp.png && rm /tmp/scrotcliptemp.png
}

# Remember more history
HISTSIZE=10000000
SAVEHIST=10000000

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

if [[ -f $HOME/.env.zsh ]]; then
  # Environment-dependent settings
  source $HOME/.env.zsh
fi
